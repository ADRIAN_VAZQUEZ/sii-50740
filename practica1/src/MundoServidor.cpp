// SERVIDOR

//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoServidor.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
bool act;
int bandera=0;

CMundoServidor::CMundoServidor()
{
	Init();

}

CMundoServidor::~CMundoServidor()
{
	
	close(open_ok);
	//close(cliente_servidor);
	//close(pasateclas);
	conexion.Close();
	comunicacion_cliente.Close();
}

void CMundoServidor::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundoServidor::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();
	if(act==true)
	esferaAD.Dibuja();
	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundoServidor::OnTimer(int value)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	//bot1->esfera=esfera;
	//bot1->raqueta=jugador1;
//codigo del bot
	/*if(bot1->accion == 1)
		OnKeyboardDown('w',0,0);
	if(bot1->accion == 0)
		OnKeyboardDown('t',0,0);
	if(bot1->accion == -1)
		OnKeyboardDown('s',0,0);*/
	
	if(act==true)
	esferaAD.Mueve(0.025f);
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		if(act==true)
		paredes[i].Rebota(esferaAD);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(act==true)
	{
	jugador1.Rebota(esferaAD);
	jugador2.Rebota(esferaAD);
	}
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		if(act==true)
		{	
		esferaAD.centro.x=0;
		esferaAD.centro.y=rand()/(float)RAND_MAX;
		esferaAD.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esferaAD.velocidad.y=2+2*rand()/(float)RAND_MAX;
		}
		puntos2++;
		datos[0]=2;
		datos[1]=puntos2;
		dato_write=write(open_ok, datos, sizeof(datos)); 
		
		
		
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		if(act==true)
		{	
		esferaAD.centro.x=0;
		esferaAD.centro.y=rand()/(float)RAND_MAX;
		esferaAD.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esferaAD.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		}
		puntos1++;
		datos[0]=1;
		datos[1]=puntos1;
		dato_write=write(open_ok, datos, sizeof(datos)); 
		

	}
		
		

	
 int puntos=puntos1+puntos2;
        if(puntos==2)
        {
           act=true;
        }

//Escritura datos SERVIDOR_CLIENTE
	fflush(stdin);
	char datosSERCLI[200];
	sprintf(datosSERCLI,"%f %f %f %f %f %f %f %f %f %f %d %d", esfera.centro.x,esfera.centro.y,jugador1.x1,jugador1.y1,jugador1.x2,jugador1.y2,jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2, puntos1, puntos2);
	comunicacion_cliente.Send(datosSERCLI, sizeof(datosSERCLI));


}

void CMundoServidor::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;
	}
}
void* hilo_comandos(void* d)
{
      CMundoServidor* p=(CMundoServidor*) d;
      p->RecibeComandosJugador();
}

void CMundoServidor::RecibeComandosJugador()
{
     while (1) {
            usleep(10);
            char cad[100];
            comunicacion_cliente.Receive(cad, sizeof(cad));
            unsigned char key;
            sscanf(cad,"%c",&key);
            if(key=='s')jugador1.velocidad.y=-4;
            if(key=='w')jugador1.velocidad.y=4;
            if(key=='l')jugador2.velocidad.y=-4;
            if(key=='o')jugador2.velocidad.y=4;
      }
}

void CMundoServidor::Init()
{
	Plano p;
	
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

	//El servidor se comunicar con el logger a traves de una FIFO	
	open_ok=open("/tmp/logger", O_WRONLY);
	if(open_ok<0){
		printf("ERROR al abrir FIFO LOGGER \n");
		}
	
	/*cliente_servidor=open("/tmp/sercli",O_WRONLY);
	if(cliente_servidor<0){
		printf("ERROR al abrir FIFO SERCLI \n");
		}
	pasateclas=open("/tmp/pasateclas", O_RDONLY);
	if(pasateclas<0){
		printf("ERROR al abrir FIFO PASATECLAS \n");
		}*/
	pthread_create(&thid1, NULL, hilo_comandos, this);
	//Comunicacion con el socket de conexion
	conexion.InitServer((char*)"127.0.0.1",12000);
	comunicacion_cliente=conexion.Accept();
	char nombrecliente[100];
	comunicacion_cliente.Receive(nombrecliente,sizeof(nombrecliente));
	printf("Se ha conectado %s\n",nombrecliente);
	
	
}
