#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
int main()
{
	int crear_ok;
	int open_ok;
	int close_ok;
	int datos[2];
	int dato_read;

	mkfifo("/tmp/logger", 0666);
	open_ok=open("/tmp/logger", O_RDONLY);
	if(open_ok<0){
		printf("ERROR al abrir FIFO \n");
		return 0;
		}
	while(1){
		dato_read= read(open_ok,datos,sizeof(datos));
		if(dato_read)
		printf("Jugador %d marca 1 punto, lleva un total de %d puntos\n", datos[0], datos[1]);
		}
	close_ok=close(open_ok);
	unlink("/tmp/logger");
	return 0;
}

