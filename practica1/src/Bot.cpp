#include <stdio.h>
#include "DatosMemCompartida.h"
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>


int main()
{

	DatosMemCompartida *bot1;
	char *aux;
	int open_ok_memoria=open("/tmp/memoria",O_RDWR,0777);
	
	aux=(char *)mmap(NULL,sizeof(*(bot1)),PROT_READ|PROT_WRITE,MAP_SHARED,open_ok_memoria,0);
	close(open_ok_memoria);
	bot1=(DatosMemCompartida *)aux;
	while(1)
	{
		if(bot1->esfera.centro.y>bot1->raqueta.y1)
			bot1->accion = 1;
		if(bot1->esfera.centro.y<bot1->raqueta.y2)
			bot1->accion=-1;
		
		usleep(25000);
	}
	munmap(aux,sizeof(*(bot1)));
return 0;

			
}
